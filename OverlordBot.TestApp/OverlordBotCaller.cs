﻿// Copyright (c) Jeffrey Jones. All rights reserved.
// Licensed under the MIT License.

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace OverlordBot.TestApp
{
    class OverlordBotCaller
    {
        protected readonly ILogger Logger;
        private readonly Core.OverlordBot _bot;

        private const string RADIO_CHECK_INPUT = "overlord dolt 1 1 radio check";
        private const string PICTURE_INPUT = "overlord dolt 1 2 request picture";
        private const string BOGEY_DOPE = "overlord dolt 1 3 bogey dope";

        public OverlordBotCaller(ILogger<OverlordBotCaller> logger, Core.OverlordBot bot)
        {
            Logger = logger;
            _bot = bot;
        }

        public async Task Run()
        {
            Console.WriteLine("Calling Overlord Bot\n");

            await CallBot(RADIO_CHECK_INPUT);
            await CallBot(PICTURE_INPUT);
            await CallBot(BOGEY_DOPE);
        }

        private async Task CallBot(string input)
        {
            Console.WriteLine($"Input: {input}");
            Console.WriteLine($"Output: {await _bot.ProcessAsync(input)}\n");
        }
    }
}
