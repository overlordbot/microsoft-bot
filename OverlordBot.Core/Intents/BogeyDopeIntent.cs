﻿// Copyright (c) Jeffrey Jones. All rights reserved.
// Licensed under the MIT License.

using Microsoft.Extensions.Logging;
using OverlordBot.Core.CognitiveModels;
using OverlordBot.Core.Repository;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OverlordBot.Core.Intents
{
    public class BogeyDopeIntent
    {
        protected readonly ILogger Logger;
        private GameStateRepository _gameState;

        public BogeyDopeIntent(ILogger<BogeyDopeIntent> logger, GameStateRepository gameState)
        {
            Logger = logger;
            _gameState = gameState;
        }

        public async Task<string> Process(AwacsCall awacsCall)
        {
            (string group, int? flight, int? plane) = awacsCall.PlayerCallsign(AwacsCall.Role.caller);

            Dictionary<string, int?> braData = await _gameState.BogeyDopeAsync(group, (int)flight, (int)plane);

            string response;
            if (braData != null)
            {

                string bearing = Regex.Replace(braData["bearing"].Value.ToString("000"), "\\d{1}", " $0");
                string range = braData["range"].Value.ToString();
                string altitude = braData["altitude"].Value.ToString("N0");
                string aspect = GetAspect(braData);

                response = $"Bra, {bearing}, {range}, {altitude}{aspect}";
            }
            else
            {
                response = "Picture is clean";
            }

            return response;
        }

        private string GetAspect(Dictionary<string, int?> braData)
        {
            if (braData["heading"].HasValue == false)
            {
                return null;
            }

            int bearing = braData["bearing"].Value;
            int heading = braData["heading"].Value;

            // Allows us to just use clockwise based positive calculations
            if (heading < bearing)
            {
                heading += 360;
            }

            string aspect;

            if (heading <= bearing + 45)
            {
                aspect = "cold";
            }
            else if (heading >= bearing + 45 && heading <= bearing + 135)
            {
                aspect = "flanking right";
            }
            else if (heading >= bearing + 135 && heading <= bearing + 225)
            {
                aspect = "hot";
            }
            else if (heading >= bearing + 225)
            {
                aspect = "flanking left";
            }
            else
            {
                aspect = null;
            }

            return ", " + aspect;

        }

    }
}
