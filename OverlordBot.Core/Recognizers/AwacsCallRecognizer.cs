﻿// Copyright (c) Jeffrey Jones. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Threading.Tasks;
using Microsoft.Azure.CognitiveServices.Language.LUIS.Runtime;
using Microsoft.Azure.CognitiveServices.Language.LUIS.Runtime.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OverlordBot.Core.CognitiveModels;

namespace OverlordBot.Core.Recognizers
{
    public class AwacsCallRecognizer
    {
        private readonly LUISRuntimeClient _recognizer;
        private readonly Guid _luisAppId;
        protected readonly ILogger Logger;

        public AwacsCallRecognizer(IConfiguration configuration, ILogger<AwacsCallRecognizer> logger)
        {
            Logger = logger;

            bool luisIsConfigured = !string.IsNullOrEmpty(configuration["LuisAppId"]) &&
                !string.IsNullOrEmpty(configuration["LuisAPIKey"]) &&
                !string.IsNullOrEmpty(configuration["LuisAPIHostName"]
                );

            if (luisIsConfigured)
            {
                _luisAppId = Guid.Parse(configuration["LuisAppId"]);
                var credentials = new ApiKeyServiceClientCredentials(configuration["LuisAPIKey"]);
                _recognizer = new LUISRuntimeClient(credentials, System.Array.Empty<System.Net.Http.DelegatingHandler>())
                {
                    Endpoint = "https://" + configuration["LuisAPIHostName"],
                };
            }
        }

        // Returns true if luis is configured in the appsettings.json and initialized.
        public bool IsConfigured => _recognizer != null;

        public async Task<AwacsCall> RecognizeAsync(string inputText)
        {
            var predictionRequestOptions = new PredictionRequestOptions
            {
                DatetimeReference = DateTime.UtcNow,
                PreferExternalEntities = true
            };

            var predictionRequest = new PredictionRequest
            {
                Query = inputText,
                Options = predictionRequestOptions
            };

            var response = await _recognizer.Prediction.GetSlotPredictionAsync(
                    _luisAppId,
                    slotName: "production",
                    predictionRequest,
                    verbose: true,
                    showAllIntents: true,
                    log: true).ConfigureAwait(false);

            return new AwacsCall(response);
        }
    }
}
