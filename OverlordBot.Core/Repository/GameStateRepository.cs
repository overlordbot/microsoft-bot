﻿// Copyright (c) Jeffrey Jones. All rights reserved.
// Licensed under the MIT License.

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Npgsql;
using System;

namespace OverlordBot.Core.Repository
{
    public partial class GameStateRepository
    {
        private readonly NpgsqlConnection Database;
        protected readonly ILogger Logger;

        public GameStateRepository(IConfiguration configuration, ILogger<GameStateRepository> logger)
        {
            Logger = logger;

            NpgsqlConnectionStringBuilder builder = new NpgsqlConnectionStringBuilder()
                    {
                        Host = configuration["TacScribeDatabaseHost"],
                        Port = int.Parse(configuration["TacScribeDatabasePort"]),
                        Database = configuration["TacScribeDatabaseName"],
                        Username = configuration["TacScribeDatabaseUsername"],
                        Password = configuration["TacScribeDatabasePassword"],
                        SslMode = (SslMode) Enum.Parse(typeof(SslMode), configuration["TacScribeDatabaseSslMode"])
                    };
            Database = new NpgsqlConnection(builder.ToString());
        }
    }
}
