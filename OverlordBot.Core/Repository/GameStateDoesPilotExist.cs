﻿// Copyright (c) Jeffrey Jones. All rights reserved.
// Licensed under the MIT License.

using Npgsql;
using System.Data.Common;
using System.Threading.Tasks;

namespace OverlordBot.Core.Repository
{
    public partial class GameStateRepository
    {
        public async Task<bool> DoesPilotExistAsync(string group, int flight, int plane)
        {
            if (Database.State != System.Data.ConnectionState.Open)
            {
                await Database.OpenAsync();
            }
            DbDataReader dbDataReader;

            string command = @"SELECT id FROM public.units WHERE (pilot ILIKE '" + $"%{group} {flight}-{plane}%' OR pilot ILIKE '" + $"%{group} {flight}{plane}%')";

            using (var cmd = new NpgsqlCommand(command, Database))
            {
                dbDataReader = await cmd.ExecuteReaderAsync();
                await dbDataReader.ReadAsync();
                if (dbDataReader.HasRows)
                {
                    dbDataReader.Close();
                    return true;
                }
                else
                {
                    dbDataReader.Close();
                    return false;
                }
            }
        }
    }
}
