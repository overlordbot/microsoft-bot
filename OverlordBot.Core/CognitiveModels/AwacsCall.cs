// Copyright (c) Jeffrey Jones. All rights reserved.
// Licensed under the MIT License.

using Microsoft.Azure.CognitiveServices.Language.LUIS.Runtime.Models;
using System;
namespace OverlordBot.Core.CognitiveModels
{
    public partial class AwacsCall
    {
        private readonly PredictionResponse _response;
        public AwacsCall(PredictionResponse response)
        {
            _response = response;
        }

        public enum Intent {
            bogeyDope,
            locationOfEntity,
            None,
            picture,
            radioCheck,
            setWarningRadius
        };

        public Intent TopIntent()
        {
            return (Intent)Enum.Parse(typeof(Intent), _response.Prediction.TopIntent, true);
        }
    }
}
