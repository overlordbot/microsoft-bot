﻿// Copyright (c) Jeffrey Jones. All rights reserved.
// Licensed under the MIT License.

namespace OverlordBot.Core.CognitiveModels
{
    // Extends the partial AwacsCall class with methods and properties that simplify accessing entities in the
    // luis results
    public partial class AwacsCall
    {
        public enum Role
        {
            caller,
            target,
        };

        // All of the following methods are dummies until https://github.com/microsoft/botframework-cli/issues/453
        // is available
        public (string groupName, int? flightNumber, int? elementNumber) PlayerCallsign(Role role)
        {
            switch (role)
            {
                case Role.caller:
                    return ("dolt", 1, 1);
                case Role.target:
                    return ("dolt", null, 2);
                default:
                    return (null, 0, 0);
            }
        }

        public string AwacsCallsign()
        {
            return "overlord";
        }

        public string AirfieldName()
        {
            return "kutaisi";
        }

        public string TankerCallsign()
        {
            return "kutaisi";
        }

        public int WarningDistance()
        {
            return 40;
        }


    }
}
