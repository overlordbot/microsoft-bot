// Copyright (c) Jeffrey Jones. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;

namespace OverlordBot.BotFramework
{
    public class OverlordBotHandler : ActivityHandler
    {
        protected readonly ILogger Logger;
        private readonly Core.OverlordBot _coreBot;

        public OverlordBotHandler(ILogger<OverlordBotHandler> logger, Core.OverlordBot coreBot) {
            Logger = logger;
            _coreBot = coreBot;

        }

        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            StringBuilder response = new StringBuilder();
            try
            {
                response = await _coreBot.ProcessAsync(turnContext.Activity.Text);
            }
            catch (Exception exception)
            {
                response.Append("we are suffering equipment failure");
                await turnContext.SendActivityAsync(MessageFactory.Text(response.ToString(), response.ToString()), cancellationToken);
                throw exception;
            }
            await turnContext.SendActivityAsync(MessageFactory.Text(response.ToString(), response.ToString()), cancellationToken);
        }
    }
}
